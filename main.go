package main

import (
	"archive/zip"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
	"time"

	"github.com/urfave/cli"
)

var (
	inputFolder  string
	outputFolder string
	fileFormat   string
	expireDays   int
)

func main() {
	app := cli.NewApp()
	app.Name = "compressor"
	app.Usage = "CLI application for compress old logs"
	app.Author = "stanislav.dev3@gmail.com"
	app.Version = "1.0.0"
	app.UsageText = "compressor [INPUT DIR PATH] [OUTPUT DIR PATH] [FILE FORMAT] [DAYS]\n " +
		"\t Example: compressor -i /var/log -o /var/log -f .log -d 5"

	app.Flags = []cli.Flag{
		cli.StringFlag{Name: "input, i", Value: ".", Usage: "Input dir path", Destination: &inputFolder,},
		cli.StringFlag{Name: "output, o", Value: ".", Usage: "Output dir path", Destination: &outputFolder,},
		cli.StringFlag{Name: "format, f", Value: ".log", Usage: "Target files type", Destination: &fileFormat,},
		cli.IntFlag{Name: "days, d", Value: 3, Usage: "Expire days number", Destination: &expireDays,},
	}

	app.Action = func(c *cli.Context) error {
		cli.DefaultAppComplete(c)
		if c.NumFlags() == 0 {
			cli.ShowAppHelpAndExit(c, 2)
		}
		return nil
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}

	if os.Args[1] == "help" {
		return
	}

	f, err := os.Open(inputFolder)
	if err != nil {
		log.Fatal(err)
	}
	files, err := f.Readdir(-1)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	var filesCount int
	var filesList string

	for _, file := range files {
		if !file.IsDir() {
			fileType := strings.Split(file.Name(), ".")
			if "."+fileType[len(fileType)-1] == fileFormat {
				modTime := file.ModTime()
				if modTime.Sub(time.Now()).Hours() >= float64(24*expireDays) {
					err = AddFileToZip(file.Name())
					if err != nil {
						log.Fatalf("Cant compress `%s` error: %s", file.Name(), err.Error())
					}
					filesCount += 1
					filesList = fmt.Sprintf("%s \n", file.Name())
				}
			}
		}
	}
	log.Printf("Job done! \n Files count: %d \n Archived files: \n %s", filesCount, filesList)
}

func AddFileToZip(filename string) error {
	outputFile := fmt.Sprintf("%s/%s.zip", outputFolder, filename)
	newFile, err := os.Create(outputFile)
	if err != nil {
		return err
	}
	defer newFile.Close()

	zipBuf := zip.NewWriter(newFile)
	defer zipBuf.Close()

	fileToZip, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer fileToZip.Close()

	info, err := fileToZip.Stat()
	if err != nil {
		return err
	}

	header, err := zip.FileInfoHeader(info)
	if err != nil {
		return err
	}

	header.Method = zip.Deflate

	writer, err := zipBuf.CreateHeader(header)
	if err != nil {
		return err
	}
	_, err = io.Copy(writer, fileToZip)
	return err
}
