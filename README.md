# LogCompressor project

Log Compressor it's a simple cli application for compressing logs files into .zip

# How to use

Example: 

```logcomressor -i /var/log -o /var/log/archive -f .log -d 3```

Where:

    --input [str], -i [str]   Input dir path (default: ".")
    --output [str], -o [str]  Output dir path (default: ".")
    --format [str], -f [str]  Target files type (default: ".log")
    --days [int], -d [int]    Expire days number (default: 3)
    --help, -h                show help
    --version, -v             print the version
